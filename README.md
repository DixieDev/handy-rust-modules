# Handy Rust Modules

A collection of standalone Rust modules that come in handy regularly. 
The benefit of storing them as single modules instead of entire Crates is that
it's a lot simpler to customise once added to your project. 

Additionally, it's a lot easier to standardise frequently used types, patterns,
and data structures when dependency source is in the same Crate you're working
on. For example, the thread-pool in this repo uses a `Queue` that can be easily
swapped out for your own implementation without damaging the thread-pool's 
fairly straight-forward interface.
