use std::io::{self, Read};
use std::convert::TryFrom;

pub enum GetchResult {
    Char(char),
    End,
    IoErr(io::Error),
    Invalid,
}

pub struct Getch {
    cb: CharBuilder,
    buf: [u8; 4],
    cursor: usize,
}

impl Getch {
    pub fn new() -> Self {
        Self {
            cb: CharBuilder::new(),
            buf: [0; 4],
            cursor: 0,
        }
    }

    pub fn getch<R: Read>(&mut self, reader: &mut R) -> GetchResult {
        // Push excess buf bytes into the char builder
        if self.cursor > 0 {
            if let Some(result) = self.read_buf() {
                return result;
            }
        }

        // Read into buf from the reader
        match reader.read(&mut self.buf) {
            Ok(0) => GetchResult::End,
            Ok(_) => {
                match self.read_buf() {
                    Some(result) => result,
                    None => GetchResult::Invalid,
                }
            },
            Err(e) => GetchResult::IoErr(e),
        }
    }

    fn read_buf(&mut self) -> Option<GetchResult> {
        while self.cursor < self.buf.len() {
            let byte = self.buf[self.cursor];
            self.cursor += 1;

            match self.cb.push_byte(byte) {
                BuildResult::Ok('\x03') => return Some(GetchResult::End),
                BuildResult::Ok(c) => return Some(GetchResult::Char(c)),
                BuildResult::InProgress => (),
                BuildResult::Invalid(_rep, _last_byte) => return Some(GetchResult::Invalid),
            }
        }
        self.cursor = 0;
        None
    }
}

#[cfg(test)]
mod getch_tests {
    use super::{Getch, GetchResult};

    #[test]
    fn getch() {
        let s = "hello 👋😄";
        let mut bytes = s.as_bytes();

        let mut reader = Getch::new();
        let mut rebuilt = String::new();

        loop {
            match reader.getch(&mut bytes) {
                GetchResult::Char(c) => rebuilt.push(c),
                GetchResult::End => break,
                GetchResult::IoErr(e) => {
                    eprintln!("IO error while calling getch: {:?}", e);
                    break;
                }
                GetchResult::Invalid => {
                    eprintln!("Error while calling getch");
                    break;
                }
            }
        }

        assert_eq!(s, rebuilt.as_str());
    }
}

enum BuildResult {
    Ok(char),
    InProgress,
    Invalid(u32, u8),
}

enum BuildState {
    Idle,
    Expecting(u32),
}

struct CharBuilder {
    state: BuildState,
    rep: u32,
}

impl CharBuilder {
    pub fn new() -> Self {
        Self {
            state: BuildState::Idle,
            rep: 0,
        }
    }

    pub fn clear(&mut self) {
        self.rep = 0;
        self.state = BuildState::Idle;
    }

    // UTF-8 Encoding table
    // 1 	0xxxxxxx 	
    // 2 	110xxxxx 	10xxxxxx 	
    // 3 	1110xxxx 	10xxxxxx 	10xxxxxx 	
    // 4 	11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
    pub fn push_byte(&mut self, byte: u8) -> BuildResult {
        match self.state {
            BuildState::Idle => {
                if byte & 0b10000000 == 0 {
                    BuildResult::Ok(byte.into())
                } else if byte & 0b11100000 == 0b11000000 {
                    let masked = byte & 0b0001111;
                    self.rep = (masked as u32) << 6;
                    self.state = BuildState::Expecting(1);
                    BuildResult::InProgress
                } else if byte & 0b11110000 == 0b11100000 {
                    let masked = byte & 0b00001111;
                    self.rep = (masked as u32) << 12;
                    self.state = BuildState::Expecting(2);
                    BuildResult::InProgress
                } else if byte & 0b11111000 == 0b11110000 {
                    let masked = byte & 0b00000111;
                    self.rep = (masked as u32) << 18;
                    self.state = BuildState::Expecting(3);
                    BuildResult::InProgress
                } else {
                    BuildResult::Invalid(self.rep, byte)
                }
            },

            BuildState::Expecting(n) => {
                if byte & 0b11000000 == 0b10000000 {
                    let masked = byte & 0b00111111;
                    let remaining = n-1;
                    self.rep |= (masked as u32) << 6 * remaining;
                    if remaining == 0 {
                        self.state = BuildState::Idle;
                        match char::try_from(self.rep) {
                            Ok(c) => BuildResult::Ok(c),
                            Err(_) => BuildResult::Invalid(self.rep, byte),
                        }
                    } else {
                        self.state = BuildState::Expecting(remaining);
                        BuildResult::InProgress
                    }
                } else {
                    self.state = BuildState::Idle;
                    BuildResult::Invalid(self.rep, byte)
                }
            },
        }
    }
}

#[cfg(test)]
mod charbuilder_tests {
    use super::{CharBuilder, BuildResult};

    #[test]
    fn build_chars() {
        let s = "hello 👋😄";
        let bytes = s.as_bytes();

        let mut cb = CharBuilder::new();
        let mut rebuilt = String::new();

        for i in 0..bytes.len() {
            match cb.push_byte(bytes[i]) {
                BuildResult::Ok(c) => rebuilt.push(c),
                BuildResult::Invalid(rep, byte) => eprintln!("Invalid codepoint: {} (0b{:b}). Last byte: 0b{:b}", rep, rep, byte),
                _ => (),
            }
        }

        assert_eq!(s, rebuilt.as_str());
    }
}
