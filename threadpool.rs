use std::thread::{self, JoinHandle};
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc, RwLock
};

type Task=Box<dyn FnOnce() + Send + Sync + 'static>;
type TaskSlot = RwLock<Option<Task>>;

/// A thread-pool with which you can queue up tasks to be executed concurrently.
/// 
/// # Example
/// ```
/// use threadpool::TaskRunner;
///
/// fn main() {
///     let mut runner = TaskRunner::new(4);
///     for _ in 0..10 {
///         runner.enqueue(move || {
///             // Do things
///         });
///     }
/// 
///     loop {
///         runner.update();
///         thread::sleep(std::time::Duration::from_millis(15));
///     }
/// }
/// ```
pub struct TaskRunner {
    thread_count: usize,
    threads: Vec<JoinHandle<()>>,
    active: Arc<Vec<AtomicBool>>,
    slots: Arc<Vec<TaskSlot>>,
    queue: Queue<Task>,
}

impl TaskRunner {
    /// Creates a new `TaskRunner` with the specified number of threads.
    pub fn new(thread_count: usize) -> Self {
        let (active, slots) = {
            let mut active = Vec::with_capacity(thread_count);
            let mut slots = Vec::with_capacity(thread_count);

            for _ in 0..thread_count {
                active.push(AtomicBool::new(false));
                slots.push(TaskSlot::new(None));
            }

            (Arc::new(active), Arc::new(slots))
        };

        let threads = Self::init_threads(thread_count, active.clone(), slots.clone());

        Self {
            thread_count,
            threads,
            active,
            slots,
            queue: Queue::new(),
        }
    }

    /// Queues up the provided task to be executed on the next available 
    /// thread. Tasks begin execution in the order they are enqueued.
    pub fn enqueue<F>(&mut self, task: F) 
    where
        F: FnOnce() + Send + Sync + 'static
    {
        self.queue.enqueue(Box::new(task));
    }

    /// Update the `TaskRunner`, allowing it to check for available worker 
    /// threads and start queued tasks. 
    pub fn update(&mut self) {
        for i in 0..self.thread_count {
            if self.queue.is_empty() {
                break;
            }

            if !self.active[i].load(Ordering::Acquire) {
                let mut slot = self.slots[i].write().unwrap();
                *slot = self.queue.dequeue();
                self.active[i].store(true, Ordering::Release);
                self.threads[i].thread().unpark();
            }
        }
    }

    /// Initialises the thread-pool
    fn init_threads(count: usize, active: Arc<Vec<AtomicBool>>, slots: Arc<Vec<TaskSlot>>) -> Vec<JoinHandle<()>> {
        let mut threads = Vec::with_capacity(count);

        for i in 0..count {
            let active = active.clone();
            let slots = slots.clone();
            threads.push(thread::spawn(move || {
                loop {
                    while !active[i].load(Ordering::Acquire) {
                        thread::park();
                    }

                    let task = slots[i].write().unwrap().take().unwrap();
                    task();
                    active[i].store(false, Ordering::Release);
                }
            }));
        }

        threads
    }
}


/// A simple queue data structure
struct Queue<T> {
    vec: Vec<T>
}

impl<T> Queue<T> {
    fn new() -> Self {
        Self { vec: Vec::new() }
    }

    fn enqueue(&mut self, data: T) {
        self.vec.push(data);
    }

    fn dequeue(&mut self) -> Option<T> {
        if self.len() > 0 {
            Some(self.vec.remove(0))
        } else {
            None
        }
    }

    #[inline]
    fn len(&self) -> usize {
        self.vec.len()
    }

    #[inline]
    fn is_empty(&self) -> bool {
        self.vec.len() == 0
    }
}

#[cfg(test)]
mod test {
    use crate::Queue;

    #[test]
    fn queue() {
        let mut queue = Queue::new();

        assert!(queue.is_empty());

        queue.enqueue(413);
        queue.enqueue(111);
        
        assert!(queue.len() == 2);
        assert!(!queue.is_empty());

        assert!(queue.dequeue().unwrap() == 413);
        assert!(queue.dequeue().unwrap() == 111);
        assert!(queue.len() == 0);
        assert!(queue.is_empty());
    }
}
